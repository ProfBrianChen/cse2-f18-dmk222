////////////
////CSE002 hw04
///
import java.util.Scanner;
//Imports the scanner class so that it can be used

public class CrapsIf{
  //Standard main method for all java programs
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in );
    //Declares a new instance of a scanner so that it may be used
    
    System.out.println("If you'd like the program to select the dice, indicate so with the integer '1'. If you'd like to roll the dice yourself, indicate so with the integer '2'.");
    //Asks the user if they'd like for the program to roll the dice or if they would like to do so
      int rollType = myScanner.nextInt();
      //Allows the user to give their response
    String rollString = "Invalid roll";
      //Casts rollString as a string and assigns it a value to avoid compiler errors
    int firstDice = 1;
      //Casts firstDice as an integer before the switch statement and assigns it a random value to avoid compiler errors
    int secondDice = 1;
      //Casts secondDice as an integer before the switch statement and assigns it a random value to avoid compiler errors
    
    if (rollType == 1){
        //For case 1, the program determines the roll
        firstDice = (int)(Math.random() * 6) + 1;
        //Assigns the first dice roll a random value between 1 and 6
        secondDice = (int)(Math.random() * 6) + 1;
        //Assigns the Second dice roll a random value between 1 and 6
    }
      else if (rollType == 2){
         System.out.println("Enter your first dice roll: ");
        //Requests the first dice roll to be converted into slang
         firstDice = myScanner.nextInt();
        //Accepts the input of a dice roll
        System.out.println("Enter your second dice roll: ");
        //Requests the second dice roll to be converted into slang
         secondDice = myScanner.nextInt();
        //Accepts the input of a dice roll
      }
      
   if (firstDice == 1 && secondDice == 1){
     //If the first dice is 1 and the second dice is 1, then this section will evaluate
      rollString = "Snake eyes";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 1 && secondDice == 2){
      //If the first dice is 1 and the second dice is 2, then this section will evaluate
      rollString = "Ace deuce";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 1 && secondDice == 3){
      //If the first dice is 1 and the second dice is 3, then this section will evaluate
      rollString = "Easy four";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 1 && secondDice == 4){
      //If the first dice is 1 and the second dice is 4, then this section will evaluate
      rollString = "Fever five";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 1 && secondDice == 5){
      //If the first dice is 1 and the second dice is 5, then this section will evaluate
      rollString = "Easy six";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 1 && secondDice == 6){
      //If the first dice is 1 and the second dice is 6, then this section will evaluate
      rollString = "Seven out";
            //Assigns the slang value to the variable rollString
    }
      
    if (firstDice == 2 && secondDice == 1){
      //If the first dice is 2 and the second dice is 1, then this section will evaluate
      rollString = "Ace Deuce";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 2 && secondDice == 2){
      //If the first dice is 2 and the second dice is 2, then this section will evaluate
      rollString = "Hard four";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 2 && secondDice == 3){
      //If the first dice is 2 and the second dice is 3, then this section will evaluate
      rollString = "Easy five";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 2 && secondDice == 4){
      //If the first dice is 2 and the second dice is 4, then this section will evaluate
      rollString = "Easy six";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 2 && secondDice == 5){
      //If the first dice is 2 and the second dice is 5, then this section will evaluate
      rollString = "Seven out";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 2 && secondDice == 6){
      //If the first dice is 2 and the second dice is 6, then this section will evaluate
      rollString = "Easy eight";
            //Assigns the slang value to the variable rollString
    }
    
      if (firstDice == 3 && secondDice == 1){
        //If the first dice is 3 and the second dice is 1, then this section will evaluate
      rollString = "Easy four";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 3 && secondDice == 2){
      //If the first dice is 3 and the second dice is 2, then this section will evaluate
      rollString = "Fever Five";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 3 && secondDice == 3){
      //If the first dice is 3 and the second dice is 3, then this section will evaluate
      rollString = "Hard six";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 3 && secondDice == 4){
      //If the first dice is 3 and the second dice is 4, then this section will evaluate
      rollString = "Seven out";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 3 && secondDice == 5){
      //If the first dice is 3 and the second dice is 5, then this section will evaluate
      rollString = "Easy eight";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 3 && secondDice == 6){
      //If the first dice is 3 and the second dice is 6, then this section will evaluate
      rollString = "Nine";
            //Assigns the slang value to the variable rollString
    }
      
    if (firstDice == 4 && secondDice == 1){
      //If the first dice is 4 and the second dice is 1, then this section will evaluate
      rollString = "Fever five";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 4 && secondDice == 2){
      //If the first dice is 4 and the second dice is 2, then this section will evaluate
      rollString = "Easy six";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 4 && secondDice == 3){
      //If the first dice is 4 and the second dice is 3, then this section will evaluate
      rollString = "Seven out";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 4 && secondDice == 4){
      //If the first dice is 4 and the second dice is 4, then this section will evaluate
      rollString = "Easy six";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 4 && secondDice == 5){
      //If the first dice is 4 and the second dice is 5, then this section will evaluate
      rollString = "Seven out";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 4 && secondDice == 6){
      //If the first dice is 4 and the second dice is 6, then this section will evaluate
      rollString = "Easy eight";
            //Assigns the slang value to the variable rollString
    }
    
      
      
    if (firstDice == 5 && secondDice == 1){
      //If the first dice is 5 and the second dice is 1, then this section will evaluate
      rollString = "Easy six";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 5 && secondDice == 2){
      //If the first dice is 5 and the second dice is 2, then this section will evaluate
      rollString = "Seven out";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 5 && secondDice == 3){
      //If the first dice is 5 and the second dice is 3, then this section will evaluate
      rollString = "Easy eight";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 5 && secondDice == 4){
      //If the first dice is 5 and the second dice is 4, then this section will evaluate
      rollString = "Nine";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 5 && secondDice == 5){
      //If the first dice is 5 and the second dice is 5, then this section will evaluate
      rollString = "Hard ten";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 5 && secondDice == 6){
      //If the first dice is 5 and the second dice is 6, then this section will evaluate
      rollString = "Yo-Leven";
            //Assigns the slang value to the variable rollString
    }
      
    if (firstDice == 6 && secondDice == 1){
      //If the first dice is 6 and the second dice is 1, then this section will evaluate
      rollString = "Seven out";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 6 && secondDice == 2){
      //If the first dice is 6 and the second dice is 2, then this section will evaluate
      rollString = "Easy eight";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 6 && secondDice == 3){
      //If the first dice is 6 and the second dice is 3, then this section will evaluate
      rollString = "Nine";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 6 && secondDice == 4){
      //If the first dice is 6 and the second dice is 4, then this section will evaluate
      rollString = "Easy ten";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 6 && secondDice == 5){
      //If the first dice is 6 and the second dice is 5, then this section will evaluate
      rollString = "Yo-Leven";
            //Assigns the slang value to the variable rollString
    }
    else if (firstDice == 6 && secondDice == 6){
      //If the first dice is 6 and the second dice is 6, then this section will evaluate
      rollString = "Boxcars";
            //Assigns the slang value to the variable rollString
    }
    
       System.out.println("The slang of your roll was: " + rollString);
    //Lets the user know what the slang of their roll is
    
    
    }
}