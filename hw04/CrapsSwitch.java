////////////
////CSE002 hw04
///
import java.util.Scanner;
//Imports the scanner class so that it can be used

public class CrapsSwitch{
  //Standard main method for all java programs
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in );
    //Declares a new instance of a scanner so that it may be used
    
    System.out.println("If you'd like the program to select the dice, indicate so with '1'. If you'd like to roll the dice yourself, indicate so with '2'.");
    //Asks the user if they'd like for the program to roll the dice or if they would like to do so
      int rollType = myScanner.nextInt();
      //Allows the user to give their response
    String rollString = "Invalid roll";
      //Casts rollString as a string and assigns it a value to avoid compiler errors
    int firstDice = 1;
      //Casts firstDice as an integer before the switch statement and assigns it a random value to avoid compiler errors
    int secondDice = 1;
      //Casts secondDice as an integer before the switch statement and assigns it a random value to avoid compiler errors
    switch (rollType){ 
        //Starts a switch statement, using the type of roll chosen to determine which statement is executed
      case 1: 
        //For case 1, the program determines the roll
        firstDice = (int)(Math.random() * 6) + 1;
        //Assigns the first dice roll a random value between 1 and 6
        secondDice = (int)(Math.random() * 6) + 1;
        //Assigns the Second dice roll a random value between 1 and 6
        break;
        //Required break statement for switches
      case 2:
        System.out.println("Enter your first dice roll: ");
        //Requests the first dice roll to be converted into slang
         firstDice = myScanner.nextInt();
        //Accepts the input of a dice roll
        System.out.println("Enter your second dice roll: ");
        //Requests the second dice roll to be converted into slang
         secondDice = myScanner.nextInt();
        //Accepts the input of a dice roll
        break;
        //Required break statement for switches
    
    default: System.out.println("Invalid option");  
        //In case they don't choose 1 or 2, this displays
        break;
         //Required break statement for switches
    }
     switch (firstDice){
         //Begins switch statement with the first dice determining 
      case 1:
         //In the first case, the first number rolled is a 1
        switch (secondDice){
            //Begins a switch statement within the switch statement with the second dice determining
          case 1:
            //In this case, the user rolled a 1 and a 1
            rollString = "Snake eyes";
            //Assigns the slang value to the variable rollString
          break; 
             //Required break statement for switches
          case 2:
             //In this case, the user rolled a 1 and a 2
            rollString = "Ace Deuce";
            //Assigns the slang value to the variable rollString
            break;
             //Required break statement for switches
          case 3:
             //In this case, the user rolled a 1 and a 3
            rollString = "Easy Four";
            //Assigns the slang value to the variable rollString
            break;
             //Required break statement for switches
          case 4:
             //In this case, the user rolled a 1 and a 4
            rollString = "Easy Five";
            //Assigns the slang value to the variable rollString
            break;
             //Required break statement for switches
          case 5:
             //In this case, the user rolled a 1 and a 5
            rollString = "Easy Six";
            //Assigns the slang value to the variable rollString
            break;
             //Required break statement for switches
          case 6:
             //In this case, the user rolled a 1 and a 6
            rollString = "Seven Out";
            //Assigns the slang value to the variable rollString
            break;
             //Required break statement for switches
        }
        break;
          //Required break statement for switches
         
       case 2:
         switch (secondDice){
          case 1:
            rollString = "Ace Deuce";
            break; 
          case 2:
            rollString = "Hard Four";
            break;
          case 3:
            rollString = "Fever Five";
            break;
          case 4:
            rollString = "Easy Six";
            break;
          case 5:
            rollString = "Seven Out";
            break;
          case 6:
            rollString = "Easy Eight";
            break;
         }
         break;
         
       case 3:
         switch (secondDice){
          case 1:
            rollString = "Easy Four";
            break; 
          case 2:
            rollString = "Fever Five";
            break;
          case 3:
            rollString = "Hard Six";
            break;
          case 4:
            rollString = "Seven out";
            break;
          case 5:
            rollString = "Easy Eight";
            break;
          case 6:
            rollString = "Nine";
            break;
         }
         break;
         
       case 4:
         switch (secondDice){
          case 1:
            rollString = "Easy Five";
            break; 
          case 2:
            rollString = "Easy Six";
            break;
          case 3:
            rollString = "Seven Out";
            break;
          case 4:
            rollString = "Hard Eight";
            break;
          case 5:
            rollString = "Nine";
            break;
          case 6:
            rollString = "Easy Ten";
            break;
         }
         break;
         
       case 5:
         switch (secondDice){
          case 1:
            rollString = "Easy Six";
            break; 
          case 2:
            rollString = "Seven Out";
            break;
          case 3:
            rollString = "Easy Eight";
            break;
          case 4:
            rollString = "Nine";
            break;
          case 5:
            rollString = "Hard Ten";
            break;
          case 6:
            rollString = "Yo-Leven";
            break;
         }
         break;
         
       case 6:
         switch (secondDice){
          case 1:
            rollString = "Seven Out";
            break; 
          case 2:
            rollString = "Easy Eight";
            break;
          case 3:
            rollString = "Nine";
            break;
          case 4:
            rollString = "Easy Ten";
            break;
          case 5:
            rollString = "Yo-Leven";
            break;
          case 6:
            rollString = "Boxcars";
            break;
         }
         default:
         //In case a roll that is greater than 6 or less than 1 is chosen, this executes
         System.out.println("Invalid roll");
         //Lets the user know their roll is invalid
           break;
         //Standard break statement for switches
    }
    
    System.out.println("The slang of your roll was: " + rollString);
    //Lets the user know what the slang of their roll is
    
    
  }
}