/////////////
//// CSE002: hw01 
/// This displays the message and signs with my ID number.
public class WelcomeClass{
  
  //Main method required for every java program
  public static void main(String[] args) {
    
    //This displays the top of the "welcome" message.
   System.out.println("  -----------"); 
    //This displays the actual message that says welcome.
    System.out.println("  | WELCOME |"); 
    //This displays the bottom line to the "welcome" message.
    System.out.println("  -----------");
      
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-D--M--K--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v ");
  } 
  }