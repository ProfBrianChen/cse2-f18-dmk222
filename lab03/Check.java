///////////
////CSE002 Check 
///
import java.util.Scanner;
//Imports the sanner class so that it can be used
public class Check{
  //Main method for every java program
  public static void main(String[] args){
  
    Scanner myScanner = new Scanner( System.in );
    //Declares an instance of a scanner so that it may accept input
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //Lets the user of the program know that they should enter the cost, and shows how they should enter it
    double checkCost = myScanner.nextDouble();
    //Accepts user input of the cost of the check
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    //Lets the user know that they should enter the cost and defines the form
    double tipPercent = myScanner.nextDouble();
    //Accepts user input of the percent they would like to tip
    tipPercent /= 100;
    //Converts the percentage tip into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    //Lets the user know where and what to enter the number of people.
    int numPeople = myScanner.nextInt();
    //Allows the system to accept the number of people as an integer
    double totalCost;
    //Establishes total cost and calls it a double
    double costPerPerson;
    //Establishes the total cost per person
    int dollars,
    dimes,
    pennies;
    //Whole dollar ammount of dollars, dimes, and pennies
    totalCost = checkCost * (1 + tipPercent);
    //Calculates total cost
    costPerPerson = totalCost / numPeople;
    //Calculates the cost per person
    dollars=(int)costPerPerson;
    //Finds the number of dollars each person must pay
    dimes=(int)(costPerPerson * 10) % 10;
    //Finds the number of dimes each person must pay
    pennies=(int)(costPerPerson * 100) % 10;
    //Finds the number of pennies each eprson must pay
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
    //Shows the user how much they have to pay
    
  } //End of main method
} //End of main class