///////////
////CSE002 Convert
///
import java.util.Scanner;
//Imports the scanner class so that it may be used
public class Convert{
  //Main method, standard for all java programs
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in );
    //Declares a new instance of a scanner so that it may accept input
    System.out.println("Enter the affected area in acres: ");
    //Requests the user to enter the affected number of acres
    double acresAffected = myScanner.nextDouble();
    //Allows the user to enter the number of aceres affected
    
    System.out.println("Enter the rainfall in the affected area in inches: ");
    //Requests the user to enter the amount of rainfall in inches
    double rainInInches = myScanner.nextDouble();
    //Allows the user to enter the amount of rainfall
    
    double squareMilesAffected = acresAffected / 640;
      //Converts the acres into square miles
      double rainFallInCubicMiles = squareMilesAffected * (rainInInches / (12 * 5280));
    /* Preforms the opperation to calculate the rain fall in cubic miles. 
    It converts the rain in inches to rain in miles, and the multiplies that by the square miles affected to calculate the rain fall in cubic miles */
    
    System.out.println("The rainfall in cubic miles is: " + rainFallInCubicMiles + ".");
    //Tells the user what the rain fall is in cubic miles
  }
}