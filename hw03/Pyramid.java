///////////
////CSE002 Pyramid
///
import java.util.Scanner;
//Imports the scanner class
public class Pyramid{
  //Main method standard for all java programs
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in );
    //Declares a new scanner, "myscanner", so that it may be used
    
    System.out.println("Please enter the length of the square side of the pyramid: ");
      //Requests the user to enter the length of the square side of the pyramid
      double sideLength = myScanner.nextDouble();
      //Allows the user to input the length of the first side
    
    System.out.println("Please enter the height of the pyramid: ");
      //Requests the user to enter the height of the pyramid
      double pyrHeight = myScanner.nextDouble();
      //Allows the user to input the length of the first side
    
    double baseArea = Math.pow(sideLength , 2);
    //Squares the base length to find the area of the base
    double pyrVolume = (baseArea * pyrHeight) / 3;
    //Calculates the volume of a pyramid
      System.out.println("The volume inside the pyramid is: " + pyrVolume + ".");
      //Tells the user what the final volume is
  }
}
