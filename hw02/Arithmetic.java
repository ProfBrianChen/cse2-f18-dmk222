///////////
////CSE002 Arithmetic
public class Arithmetic{
  //Main method, standard for java programs
  public static void main(String args[]){
    
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//Cost per belt
double beltCost = 33.99;

//The tax rate
double paSalesTax = 0.06;
    
    //Total cost of pants
    double totalPantsCost = numPants * pantsPrice;
    //Total cost of shirts
    double totalShirtsCost = numShirts * shirtPrice;
    //Total cost of belts
    double totalBeltsCost = numBelts * beltCost;
    //Displays total cost of pants without tax
    System.out.println("The total cost of pants is " +  totalPantsCost +  " without tax.");
    //Displays total cost of shirts without tax
    System.out.println("The total cost of shirts is " +  totalShirtsCost +  " without tax.");
    //Displays total cost of belts without tax
    System.out.println("The total cost of belts is " +  totalBeltsCost +  " without tax.");
    
    
    //Calculates total cost of pants with tax
    double pantsTax = totalPantsCost * paSalesTax;
    double totalPantsCostTax = pantsTax + totalPantsCost;
    
    //Calculates total cost of shirts with tax
    double shirtsTax = totalShirtsCost * paSalesTax;
    double totalShirtsCostTax = shirtsTax + totalPantsCost;
    
    //Calculates total cost of belts with tax
    double beltsTax = totalBeltsCost * paSalesTax;
    double totalBeltsCostTax = beltsTax + totalPantsCost;
      
    //Displays total cost of pants with tax
    System.out.println("The total cost of pants with tax is " + totalPantsCostTax + ".");   
    //Displays total cost of shirts with tax
    System.out.println("The total cost of shirts with tax is " + totalShirtsCostTax + ".");   
    //Displays total cost of belts with tax
    System.out.println("The total cost of belts with tax is " + totalBeltsCostTax + ".");
    
    
    //Calcuates total cost of purchase without tax
    double sumCost = numPants * pantsPrice + numShirts * shirtPrice + numBelts * beltCost;
    //Displays total cost of purchase without tax
    System.out.println("The total cost of the purchase without tax is " + sumCost + ".");
      
    
    //Calculates total sales tax
    double totalTax = sumCost * paSalesTax;
    //Displays total sales tax
    System.out.println("The ammount the tax costs is " + totalTax + ".");
      
    //Calculates total cost
    double totalCost = totalTax + sumCost;
    //Rounds total cost to the nearest hundreth.
    int newCost = (int) (totalCost * 100);
      double newNewCost = (double) newCost / 100;
    System.out.println(newNewCost);
    
    //Displays final total cost
    System.out.println("The total cost for the entire purchase is " + newNewCost + ".");
    

   
  }
}