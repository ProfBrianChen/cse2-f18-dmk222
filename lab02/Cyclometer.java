/////////////
//// CSE02 Cyclometer
/// This records bicycle data
public class Cyclometer{
  //Main method required for every java program
  
  public static void main(String[] args) {
    
    int secsTrip1 = 480; //This establishes the number of seconds in trip one as a variable and assigns it an integer.
    int secsTrip2 = 3220;  //This establishes the number of seconds in trip two as a variable and assigns it an integer.
    int countsTrip1 = 1561;  //This establishes the number of counts in trip one as a variable and assigns it an integer.
    int countsTrip2 = 9037;  //This establishes the number of counts in trip two as a variable and assigns it an integer.
  
    double wheelDiameter = 27.0, //This establishes the diameter of the wheel as a varible and I assigned the correct measurement of the wheel.
    PI = 3.1415926535, //This establishes PI as a variable. It also says what PI is.
    feetPerMile = 5280, //This establishes the number of feet in a mile as a variable and then as a number. It will be used for conversion purposes.
    inchesPerFoot = 12, //This establishes the number of inches in a foot for conversion purposes.
    secondsPerMinute = 60; //This establishes the number of seconds in a minute as a variable and gives it a number.
      
    double distanceTrip1, distanceTrip2, totalDistance; //Establishes trip distances and total distance as variables.double
      
       System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1+" counts."); //This displays the number of minutes and counts that trip 1 took.
	      System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had "+ countsTrip2+" counts."); //This displays the number of minutes and counts that trip 2 took.
   
  distanceTrip1 = countsTrip1 * wheelDiameter * PI; //This converts the number of counts into the total distance using the formula for circumfrence, which is PI times the diameter.
	distanceTrip1/=inchesPerFoot * feetPerMile; // Gives distance in miles
	distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //This uses the circumfrence formula to turn the counts of trip 2 into the total distance and then converts it into miles.
	totalDistance = distanceTrip1 + distanceTrip2; //This calculates the toal distance.
  
  //Prints out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");



      
  
  
  
  } //End of main method
  
} //End of main class
  