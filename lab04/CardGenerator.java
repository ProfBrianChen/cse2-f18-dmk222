///////////
////CSE002 lab04
///

public class CardGenerator{
  //Standard main method for all java programs
  
  public static void main(String[] args){

    
    int card = (int)(Math.random() * 52) + 1;
      //This is the random number generator, it finds a number between 1 and 52
    String cardSuit = "PlaceHolder";
      //This establishes "cardSuit" as a string
    String cardId = "PlaceHolder";
      //This establishes "cardId" as a string
    
    if (card <= 13){
    cardSuit = "Spades";
      //This section says that if the card is less than thirteen, then the string
    }
      
    else if (card <= 26){
      cardSuit = "Clubs";
    }
    
    else if (card <= 39){
      cardSuit = "Diamonds";
    }
    
    else if (card <= 52){
      cardSuit = "Hearts";
    }
    
    if ((card % 13) == 1){
      cardId = "Ace";
    }
    
    if ((card % 13) == 2){
      cardId = "Two";
    }
    
    if ((card % 13) == 3){
      cardId = "Three";
    }
    
    if ((card % 13) == 4){
      cardId = "Four";
    }
    
    if ((card % 13) == 5){
      cardId = "Five";
    }
    
    if ((card % 13) == 6){
      cardId = "Six";
    }
    
    if ((card % 13) == 7){
      cardId = "Seven";
    }
    
    if ((card % 13) == 8){
      cardId = "Eight";
    }
    
    if ((card % 13) == 9){
      cardId = "Nine";
    }
    
    if ((card % 13) == 10){
      cardId = "Ten";
    }
    
    if ((card % 13) == 11){
      cardId = "Jack";
    }
    
    if ((card % 13) == 12){
      cardId = "Queen";
    }
    
    if ((card % 13) == 0){
      cardId = "King";
    }
    
    System.out.println("Your card is the " + cardId + " of " + cardSuit + "." );
    
    //System.out.println(card % 13);
  }
}