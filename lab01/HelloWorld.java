///////////
//// CSE002 Hello World
///
public class HelloWorld{
  //Main method, standard for all java programs
  public static void main(String args[]){
    //Prints "Hello, World!" to terminal window
    System.out.println("Hello, World");
  }
}
